<?php
include "Services/Twilio/Capability.php";
 
// AccountSid and AuthToken can be found in your account dashboard
$accountSid = "AC92b44d74c4ec5da297cb5fa1a39ba091"; 
$authToken = "af98d6fdf3201d9800579a3a0e7f040f"; 
 
// The app outgoing connections will use:
//$appSid = "APabe7650f654fc34655fc81ae71caa3ff"; 
$appSid = "AP4600adb42d62380c87365344ab1726e3";
// The client name for incoming connections:
$clientName = $_REQUEST["clientName"];
 
$capability = new Services_Twilio_Capability($accountSid, $authToken);
 
// This allows incoming connections as $clientName: 
$capability->allowClientIncoming($clientName);
 
// This allows outgoing connections to $appSid with the "From" 
// parameter being the value of $clientName 
$capability->allowClientOutgoing($appSid, array(), $clientName);
 
// This returns a token to use with Twilio based on 
// the account and capabilities defined above 
$token = $capability->generateToken();
 
echo $token;
?>
